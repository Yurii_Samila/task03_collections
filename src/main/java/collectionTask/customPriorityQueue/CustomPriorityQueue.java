package collectionTask.customPriorityQueue;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomPriorityQueue<E> extends AbstractQueue<E> {

  private int size;
  private List<E> list = new ArrayList<>(size);

  public CustomPriorityQueue(){
    this.size = 16;
  }
  public CustomPriorityQueue (int size){
    this.size = size;
  }

  @Override
  public Iterator<E> iterator() {
    return list.iterator();
  }

  @Override
  public int size() {
    return list.size();
  }

  @Override
  public boolean offer(E e) {
    return list.add(e);
  }

  @Override
  public E poll() {
    E element;
    if (list.isEmpty()){
      return null;
    }
    element = list.get(0);
    list.remove(0);
    return element;
  }

  @Override
  public E peek() {
    if (list.isEmpty()){
      return null;
    }
    return list.get(0);
  }
}
