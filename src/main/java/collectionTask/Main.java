package collectionTask;

import collectionTask.customBinaryTree.CustomBinaryTreeMap;
import collectionTask.customDeque.CustomDeque;

public class Main {
  public static void main(String[] args) {
    CustomDeque<String> customDeque = new CustomDeque<>();
    customDeque.addFirst("AAA");
    customDeque.addFirst("BBB");
    customDeque.addFirst("CCC");
    customDeque.addFirst("ZZZ");
    customDeque.addFirst("45");
    customDeque.addFirst("BBB");
    System.out.println(customDeque);
    customDeque.removeFirstOccurrence("BBB");
    System.out.println(customDeque);
    CustomBinaryTreeMap<Integer, String> map = new CustomBinaryTreeMap<>();
    map.put(8, "aa");
    map.put(9, "cc");
    map.put(2, "vv");
    map.put(12, "nn");
    System.out.println(map.size());
    System.out.println(map);
    System.out.println(map.get(2));

  }
}
