package collectionTask.customBinaryTree;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.*;

public class CustomBinaryTreeMap<K extends Comparable<K>, V> implements Map<K, V> {

  private int size;
  private Node<K, V> root;
  Comparator<? super K> comparator;

  private static class Node<K, V> implements Map.Entry<K, V> {

    K key;
    V value;
    Node<K, V> left;
    Node<K, V> right;
    Node<K, V> root;

    @Override
    public String toString() {
      return "Node{" +
          "key=" + key +
          ", value=" + value +
          ", left=" + left +
          ", right=" + right +
          '}';
    }

    public Node(){

    }
    public Node(K key, V value) {
      this.key = key;
      this.value = value;
    }

    public Node(K key, V value, Node<K, V> root) {
      this.key = key;
      this.value = value;
      this.root = root;
    }

    @Override
    public K getKey() {
      return null;
    }

    @Override
    public V getValue() {
      return null;
    }

    @Override
    public V setValue(V value) {
      return null;
    }
  }


  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public boolean containsKey(Object key) {
    return false;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  public Node<K, V> getEntry(Object key) {
    Node<K, V> currentNode = root;
    Comparable<K> comparable = (Comparable<K>)key;
    while (currentNode != null){
      int i = comparable.compareTo(currentNode.key);
      if (i < 0){
        currentNode = currentNode.left;
      }else if (i > 0){
        currentNode = currentNode.right;
      }else
        return currentNode;
    }
    return null;
  }

  @Override
  public V get(Object key) {
    Node<K, V> entry = getEntry(key);
    return (entry == null ? null : entry.value);
  }

  @Override
  public V put(K key, V value) {
    Node<K, V> newNode = new Node<>(key, value);
    if (root == null){
      root = newNode;
      size++;
    }else {
      Node<K, V> current = root;
      Node parent;
      while (true){
        Comparable<K> currentKey = current.key;
        int compareResult = currentKey.compareTo(key);
        parent = current;
        if (compareResult > 0){
          current = current.left;
          if (current == null){
            parent.left = newNode;
            newNode.root = parent;
            size++;
            return null;
          }
        }else if (compareResult < 0){
          current = current.right;
          if (current == null){
            parent.right = newNode;
            newNode.root = parent;
            size++;
            return null;
          }
        }
        else {
          current.setValue(value);
        }
      }
    }
    return null;
}

  @Override
  public V remove(Object key) {
    return null;
  }

  final int compare(Object k1, Object k2) {
    return comparator==null ? ((Comparable<? super K>)k1).compareTo((K)k2)
        : comparator.compare((K)k1, (K)k2);
  }
//--------------------ANOTHER TRY-----------------------------
//  @Override
//  public V remove(Object key) {
//    Node<K, V> current = root;
//    Node<K, V> parent = root;
//    boolean isLeftChild = true;
//
//    while (current.key != key){
//      parent = current;
//      Comparable<K> currentKey = current.key;
//      int compareResult = currentKey.compareTo((K) key);
//      if (key < current.key){
//
//      }
//    }
//    return null;
//  }
//--------------------------------------------------------------
  private Node<K, V> findSuccessor(Node<K, V> delNodeRightHeir) {
    while (delNodeRightHeir.left != null) {
      delNodeRightHeir = delNodeRightHeir.left;
    }
    return delNodeRightHeir;
  }

  public Node findNode(K key) {
    Node<K, V> currentNode = root;
    if (currentNode == null) {
      return null;
    }
    while (currentNode.key != key) {
      Comparable<K> currentKey = currentNode.key;
      int compare = currentKey.compareTo(key);
      if (compare > 0) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
    }
    return currentNode;
  }

  public void removeNode(K key) {
    Node<K, V> delNode;
    Node<K, V> ancestor;
    Node<K, V> parent;
    Node<K, V> ancestorSuccessor;
    delNode = findNode(key);
    ancestor = delNode.root;

    if (delNode.left == null && delNode.right == null) {
      if (ancestor == null) {
        root = null;
        size--;
      } else {
        if (ancestor.right == delNode) {
          ancestor.right = null;
        } else {
          ancestor.left = null;
        }
        size--;
      }

    } else if ((delNode.left != null && delNode.right == null)
        || (delNode.left == null && delNode.right != null)) {
      if (ancestor == null) {
        if (delNode.left != null) {
          root = delNode.left;
          root.root = null;
          size--;
        } else {
          root = delNode.right;
          root.root = null;
          size--;
        }
      } else {
        if (ancestor.right == delNode && delNode.left != null) {
          ancestor.right = delNode.left;
          size--;
        } else if (ancestor.right == delNode && delNode.right != null) {
          ancestor.right = delNode.right;
          size--;
        } else if (ancestor.left == delNode && delNode.left != null) {
          ancestor.left = delNode.left;
          size--;
        } else if (ancestor.left == delNode && delNode.right != null) {
          ancestor.left = delNode.right;
          size--;
        }

      }

    } else {

      if (delNode.right.left == null) {
        parent = delNode.right;
        parent.root = ancestor;
        parent.left = delNode.left;
        delNode.left.root = parent;

        if (ancestor.right == delNode) {

          ancestor.right = parent;
          size--;

        } else if (ancestor.left == delNode) {

          ancestor.left = parent;
          size--;
        }

      } else if (delNode.right.left != null) {

        parent = findSuccessor(delNode.right);
        ancestorSuccessor = parent.root;
        parent.root = ancestor;
        parent.left = delNode.left;
        parent.right = delNode.right;
        delNode.left.root = parent;
        ancestorSuccessor.left = null;
        ancestorSuccessor.left = parent.right;

        if (parent.right == null) {

          if (ancestor.right == delNode) {
            ancestor.right = parent;
            size--;

          } else if (ancestor.left == delNode) {
            ancestor.left = parent;
            size--;
          }

        } else if (parent.right != null) {

          if (ancestor.right == delNode) {
            ancestor.right = parent;
            parent.left = delNode.left;
            size--;

          } else if (ancestor.left == delNode) {
            ancestor.left = parent;
            parent.left = delNode.left;
            size--;
          }
        }
      }
    }
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {

  }

  @Override
  public void clear() {

  }

  @Override
  public Set<K> keySet() {
    return null;
  }

  @Override
  public Collection<V> values() {
    return null;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    return null;
  }

  @Override
  public String toString() {
    return "CustomBinaryTreeMap{" +
        "root=" + root +
        '}';
  }
}
